const HR_EMAIL="hr@cronj.com";
const HR_PASS="hrcronj";
const WRONG_EMAIL_PASSWORD="Wrong Email OR Password!";
const INVALID_EMAIL="Not an email! Please insert a valid email.";
const UNKNOWN_ERROR="Something went wrong !";

$(document).ready(function(e){
	$("#homeLoginBlock").submit(function(e){
		console.log("clicked");
		e.preventDefault();
		validateUser();
	});
});
function validateUser() {
	var userEmail=$("#loginEmail").val();
	var userPass=$("#loginPass").val();
	if(!isEmailCorrect(userEmail)){
		console.log("wrong");
		$("#loginEmailIcon").html("Enter a valid Email.");
		$("#loginEmailIcon").css("display","block");
		$("#loginEmail").focus();
		return;
	}
	if(userEmail==HR_EMAIL && userPass==HR_PASS) {
		logInHR();
	}
	else{
		if(sessionStorage.totalUsers){
			logInUser(userEmail,userPass);
		}else{
			$("#loginEmailIcon").html("User does not match.");
			$("#loginEmailIcon").css("display","block");
		}	
	}
}
function logInHR(){
	if(sessionStorage.totalUsers == undefined){
		sessionStorage.totalUsers=0;
	}
	sessionStorage.currentUser=HR_EMAIL;		
	sessionStorage.setItem("beforeHrHome","x5x5x5x");
	redirectToPage('hrHome.html');
}
function logInUser(userEmail,userPass){
	var total=sessionStorage.totalUsers;
	for(var i=1;i<=total;i++){
		var usersObj=JSON.parse(sessionStorage.getItem("uid" + i));
		if(usersObj.email==userEmail && usersObj.password==userPass){
			sessionStorage.currentUser=userEmail;
			sessionStorage.setItem("beforeUserHome",'x5x5x5x');
			redirectToPage('userHome.html');
			return;
		}
	}
	$("#loginEmailIcon").html("Password does not match.");
	$("#loginEmailIcon").css("display","block");
}