var loggedInUser;
var loggedInUserId;
const TAB_COLOR_NORMAL="#DE78ED";
const TAB_COLOR_SELECTED="#8b159d";
$(document).ready(function(e){
	if(sessionStorage.getItem('beforeUserHome') != 'x5x5x5x'){
		redirectToPage('home.html');
		return;
	}	
	var current=sessionStorage.currentUser;
	$("#profileEmail").html(current);
	for(var i=1;i<=sessionStorage.totalUsers;i++){
		var usersObj=JSON.parse(sessionStorage.getItem("uid" + i));
		if(usersObj.email==current){
			loggedInUser=usersObj;
			loggedInUserId="uid" + i;
			break;
		}
	}
	var noComment=true;
	$("#navToProfile").css("background",TAB_COLOR_SELECTED);
	$("#profileName").html(loggedInUser.name);
	$("#profileMobile").html(loggedInUser.mobile);
	for(var i=0;i<loggedInUser.comment.length;i++){
		console.log(loggedInUser.comment[i]);
		var $childComments=$("<li></li>").text(loggedInUser.comment[i]);
		$childComments.appendTo("#profileComment");
		noComment=false;
	}
	if(noComment){
		var $childComments=$("<p></p>").text("No comments yet!");
		$childComments.appendTo("profileComment");
	}
	// $("#commentDiv").height($("#profileComment").height());
	$("#navToProfile").click(function(){
		showTab("#userProfile","#navToProfile");
	});
	$("#navToPassword").click(function(){
		showTab("#userPassword","#navToPassword");
	});
	$("#changePassword").submit(function(e){
		e.preventDefault();
		changePass();
	});
	$("#userLogOut").click(function(){
		logOutUser();
	});
});
function showTab(option,clicked){
	hideAllTabs();
	resetAllTabColor();
	setTabColor(clicked);
	$(option).css({
		"display":"block",
		"visibility":"visible"
	});
}
function hideAllTabs(){
	var tabs=["#userProfile","#userPassword"];
	for(var i=0;i<2;i++){
		$(tabs[i]).css({
			"display":"none",
			"visibility":"hidden"
		});
	}
}
function resetAllTabColor(){
	$("#navToProfile").css("background",TAB_COLOR_NORMAL);
	$("#navToPassword").css("background",TAB_COLOR_NORMAL);
}
function logOutUser(){
	sessionStorage.currentUser="loggedOut";
	sessionStorage.beforeUserHome='0';
	redirectToPage('home.html');
}
function changePass(){
	var oldPass=$("#oldPassword").val();
	var newPass=$("#newPassword").val();
	var confNewPass=$("#confNewPassword").val();
	$("#oldPasswordIcon").css("display","none");
	$("#newPasswordIcon").css("display","none");
	if(oldPass==loggedInUser.password){
		if(oldPass==confNewPass){
			loggedInUser.pass=newPass;
			sessionStorage.setItem(loggedInUserId,JSON.stringify(loggedInUser));
			successMessage("#passwordMessage");
			$("#newPassword").val("");
			$("#oldPassword").val("");
			$("#confNewPassword").val("");
		}else{
			$("#newPasswordIcon").css("display","block");
			$("#newPassword").val("");
			$("#confNewPassword").val("");
		}	
	}else{
		$("#oldPasswordIcon").css("display","block");
		$("#newPassword").val("");
		$("#oldPassword").val("");
		$("#confNewPassword").val("");
	}
}			