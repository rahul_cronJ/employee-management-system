function isEmailCorrect(email){
	if(!email.includes('@'))
		return false;
	var part=email.split('@');
	if(part.length!=2)
		return false;
	if(part[0].length<1||part[1].length<4)
		return false;
	if(!part[1].includes("."))
		return false;
	var regEx=new RegExp("[^a-zA-Z0-9._]");
	if(regEx.test(part[0]))
		return false;
	if(regEx.test(part[1]))
		return false;
	return true;
}
function isNameCorrect(name){
	var regExName=new RegExp("[^a-zA-Z ]");
	if(regExName.test(name))
		return false;
	return true;
}
function isMobileCorrect(mobile){
	var regExMobile=new RegExp("[1-9][0-9]{9}");
	if(mobile.length>10 || mobile.length<10)
		return false;
	if(regExMobile.test(mobile))
		return true;
	return false;
}
function isInputBlank(inputText){
	if(inputText==null || inputText=="")
		return true;
	var realText=inputText.trim();
	if(realText.length==0)
		return true;
	return false;
}
function successMessage(id){
	$(id).css("visibility","visible");
	setTimeout(function(){
		$(id).css("visibility","hidden");
	},5000);
}
function redirectToPage(pageTitle){
	window.location=pageTitle;
}
function setTabColor(tabClicked){
	$(tabClicked).css("background",TAB_COLOR_SELECTED);
}