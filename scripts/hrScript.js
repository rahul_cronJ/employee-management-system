var currentTotalUsers=1;
const TAB_COLOR_NORMAL="#DE78ED";
const TAB_COLOR_SELECTED="#8b159d";
$(document).ready(function (e){
	if(sessionStorage.getItem("beforeHrHome")==undefined)
		return redirectToPage('home.html');
	$("#totalUsers").html(sessionStorage.totalUsers);
	$("#navHrToAddUser").css('background',TAB_COLOR_SELECTED);
	$("#addUser").submit(function(e){
		e.preventDefault();
		addNewEmp();
	});
	$("#searchUser").submit(function(e){
		e.preventDefault();
		searchUser();
	});
	$("#commentUser").submit(function(e){
		e.preventDefault();
		makeComment();
	});
	$(".navTabs").click(function(){
		var toTab=$(this).attr('id');
		var tabBlock={navHrToAddUser:"#addUser",navHrToSearch:"#listUser",navHrToComment:"#commentUser"};
		navigateHrTo(tabBlock[toTab],"#"+toTab);
	});
	$("#hrLogOut").click(function(){
		hrLogOut();
	});
});
function addNewEmp() {
	var userData={email:"",password:"",name:"",mobile:"",comment:[]};
	userData.email=$("#addEmail").val();
	userData.password=$("#addPass").val();
	userData.name=$("#addName").val();
	userData.mobile=$("#addMobile").val();
	hideBlock("#addNameIcon");
	hideBlock("#addMobileIcon");
	hideBlock("#addEmailIcon");
	var wrongInput=false;
	if(!isNameCorrect(userData.name)){
		showBlock("#addNameIcon");
		$("#addName").focus();
		wrongInput=true;
	}
	if(!isMobileCorrect(userData.mobile)){
		showBlock("#addMobileIcon");
		$("#addMobile").focus();
		wrongInput=true;
	}
	if(!isEmailCorrect(userData.email)){
		showBlock("#addEmailIcon");
		$("#addEmail").focus();
		wrongInput=true;
	}
	if(wrongInput)
		return;
	var lastCount=parseInt(sessionStorage.totalUsers);
	if(userAlreadyExist(userData.email,lastCount)){
		$("#addEmailIcon").html("THIS USER EXISTS ALREADY");
		showBlock("#addEmailIcon");
		return;
	}
	sessionStorage.setItem("uid"+(lastCount+1),JSON.stringify(userData));
	sessionStorage.setItem('totalUsers',lastCount+1);
	$("#totalUsers").html(lastCount+1);
	clearAddForm();
	successMessage("#addedMessage");
	return;
}
function clearAddForm(){
	$("#addEmail").val("");
	$("#addPass").val("");
	$("#addName").val("");
	$("#addMobile").val("");
}
function hrLogOut(){
	redirectToPage('home.html');
}
function searchUser(){
	var searchPattern=$("#searchText").val();
	for(var i=1;i<currentTotalUsers;i++){
		var listItem=$('#uid'+i).html();
		if(!(listItem.includes(searchPattern))){
			$("#uid"+i).css({
				"display":"none",
				"visibility":"hidden"
			});
		}else{
			$("#uid"+i).css({
				"display":"block",
				"visibility":"visible"
			});
		}
	}	
}
function makeComment(){
	var userMail=$("#userCommentEmail").val();
	var userComment=$("#userComment").val();
	if(isInputBlank(userComment)){
		alert("Please enter a comment first!");
		return;
	}
	var total=parseInt(sessionStorage.totalUsers);
	var isUserFound=false;
	for(var i=1;i<=total;i++){
		var userObj=JSON.parse(sessionStorage.getItem("uid" + i));
		if(userObj.email==userMail){
			userObj.comment[userObj.comment.length]=userComment;
			isUserFound=true;
			sessionStorage.setItem("uid" + i,JSON.stringify(userObj));
			successMessage("#commentMessage");
			$("#userComment").val("");
			return;
		}
	}
	alert("User does not exist!");
}
function navigateHrTo(option,tabClicked){
	hideAllTab();	
    resetAllTabColor();
	setTabColor(tabClicked);
	updateDataList(option);
	if(option=="#commentUser"){
		$("#commentProfile").css({
			"visibility":"hidden",
			"display":"none"
		});
		$("#userCommentEmail").css("visibility","visible");
	}
	showTab(option);
}
function updateDataList(option){
	if(option=="#listUser" || option=="#commentUser"){
		var total=parseInt(sessionStorage.totalUsers);
		for(currentTotalUsers;currentTotalUsers<=total;currentTotalUsers++){
			var userObj=JSON.parse(sessionStorage.getItem("uid" + currentTotalUsers));
			var idL=parseInt(currentTotalUsers);	
			var $newEmp=$("<p></p>").text(userObj.email);
			$newEmp.attr("id","uid"+idL);
			$newEmp.on('click',function(){
				var x=$(this).attr('id');
				commentOn(x);
			});
			$newEmp.css({
				"margin":"auto",
				"margin-top":"1%",
				"cursor":"pointer",
				"width": "40%",
				"border": "0px solid #abcdaa",
				"background": "#e5fff9",
				"font-size": "13px",
				"padding": "1% 2%",
				"color": "#AB00A8",
				"font-family": '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
				"text-align": "center",
				"letter-spacing": "2px"
			});
			$newEmp.appendTo("#userList");
			var $newData=$("<option></option>",{
				"value":userObj.email
			});
			$newData.appendTo("#dataListComment");
		}	
	}
}
function commentOn(id){
	var selectedUser=JSON.parse(sessionStorage.getItem(id));
	$("#userCommentEmail").val(selectedUser.email);
	hideAllTab();
	resetAllTabColor();
	setTabColor("#navHrToComment");
	$("#commentProfileName").html(selectedUser.name);
	$("#commentProfileMobile").html(selectedUser.mobile);
	$("#commentProfileEmail").html(selectedUser.email);
	$("#commentProfileComments").empty();
	for(var i=0;i<selectedUser.comment.length;i++){
		var $newCommnet=$("<li></li>").text(selectedUser.comment[i]);
		$newCommnet.appendTo("#commentProfileComments");
	}
	$("#userCommentEmail").css("visibility","hidden");
	showTab("#commentUser");
	$("#commentProfile").css({
		"margin":"auto",
		"visibility":"visible",
		"display":"block",
		"width":"60%",
	});
}
function hideAllTab(){
	var tabIds=["#addUser","#listUser","#commentUser"];
	for(var i=0;i<3;i++){
		$(tabIds[i]).css({
			"display":"none",
			"visibility":"hidden"
		});
	}
	$("#commentProfile").css("display","none");
}
function showTab(option){
	$(option).css({
		"display":"block",
		"visibility":"visible"
	});
	if(option=="#addUser"){
		$("#addName").focus();
	}
	if(option=="#listUser"){
		$("#searchText").focus();
	}
	if(option=="#commentUser"){
		$("#userCommentEmail").focus();
	}
}
function resetAllTabColor(){
	$("#navHrToAddUser").css("background",TAB_COLOR_NORMAL);
	$("#navHrToSearch").css("background",TAB_COLOR_NORMAL);
	$("#navHrToComment").css("background",TAB_COLOR_NORMAL);	
}
function userAlreadyExist(userEmail,lastCount){
	for(var i=1;i<=lastCount;i++){
		var userOb=JSON.parse(sessionStorage.getItem("uid"+i));
		if(userOb.email==userEmail){
			return true;
		}
	}
	return false;
}
function hideBlock(id){
	$(id).css({
		"visibility":"hidden",
		"display":"none"
	});
}
function showBlock(id){
	$(id).css({
		"visibility":"visible",
		"display":"block"
	});
}